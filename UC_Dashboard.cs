﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Ezgi Özdikyar
*  @number  : 152120171104
*  @mail    : ezgiozdikyar@gmail.com
*  @date    : 03.06.21
*  @brief   : This class was created for Dashboard.
*/
namespace OnlineBookStore
{
    public partial class UC_Dashboard : UserControl
    {
        /// <summary>
        /// This parameter is constructor.
        /// </summary>
        public UC_Dashboard()
        {
            InitializeComponent();
        }
    }
}
