﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
/**
*  @author  : Ömer Faruk LALE
*  @number  : 152120181039
*  @mail    : omerlaleee@gmail.com
*  @date    : 03.06.21
*  @brief   : This class was created for database connection. 
*/
namespace OnlineBookStore
{
    class DbConnection
    {
        private SqlConnection connect;
        /// <summary>
        /// This function is getter and setter.
        /// </summary>
        public SqlConnection Connect { get => connect; set => connect = value; }
        /// <summary>
        ///  Database connection function.
        /// </summary>
        public void Connection()
        {
            //enter here the connection string after creating database
            var connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BookStore;Integrated Security=True";
            connect = new SqlConnection(connectionString);
        }
        /// <summary>
        ///  Database open connection function.
        /// </summary>
        public void Open()
        {
            connect.Open();
        }
        /// <summary>
        ///  Database close connection function.  
        /// </summary>
        public void Close()
        {
            connect.Close();
        }
    }
}
