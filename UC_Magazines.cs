﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Ahmet Halim UZUN
*  @number  : 152120181048
*  @mail    : ahmthlmzn@gmail.com
*  @date    : 03.06.21
*  @brief   : This class for all Magazine User Control. 
*/
namespace OnlineBookStore
{
    public partial class UC_Magazines : UserControl
    {
        /// <summary>
        /// This parameter is constructor.
        /// </summary>
        public UC_Magazines()
        {
            InitializeComponent();
        }
    }
}
